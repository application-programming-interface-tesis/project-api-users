#!/usr/bin/env python
# coding: utf-8

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.middlewares.error_handler import ErrorHandler
from app.routers import users_router

app = FastAPI()
app.title = "API Authenticacion of User"
app.version = "0.0.1"

# Configurar CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Dominios permitidos
    allow_credentials=True,
    allow_methods=["*"],  # Métodos permitidos
    allow_headers=["*"],  # Encabezados permitidos
)

app.include_router(users_router.user)
app.add_middleware(ErrorHandler)

if __name__ == "__main__":
    import uvicorn    
    
    uvicorn.run(app, host="0.0.0.0", port=8000)
