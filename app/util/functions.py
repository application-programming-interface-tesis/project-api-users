import hashlib
import json
import os

def load_config_user(ruta_archivo):
    try:
        with open(ruta_archivo, 'r') as archivo:
            configuracion = json.load(archivo)
        return configuracion
    except FileNotFoundError:
        print(f"Error: El archivo '{ruta_archivo}' no fue encontrado.")
        return None
    except json.JSONDecodeError:
        print("Error: No se pudo decodificar el contenido del archivo JSON")
        return None
    
    
def hash_password(password, salt=None):
    # Genera un salt aleatorio
    if salt is None:
        salt = os.urandom(16).hex()

    # Combina la contraseña y el salt y aplica una función hash
    hashed_password = hashlib.sha256((password + salt).encode()).hexdigest()
    return hashed_password, salt



def verify_password(entered_password, stored_password, salt):
    # Verifica la contraseña ingresada con la almacenada
    entered_password_hash = hashlib.sha256((entered_password + salt).encode()).hexdigest()
    return entered_password_hash == stored_password
    