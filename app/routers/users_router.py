from fastapi import APIRouter
from app.models.request import ChangePassword, CreateUser, DeleteUser, ValidateUser, ResetPassword
from app.services.change_password import change_password_etl
from app.services.create_users import create_user_etl
from app.services.delete_users import delete_user_etl
from app.services.query_users import query_users_etl
from app.services.reset_password import reset_password_etl
from app.services.validate_user import validate_user_etl

user = APIRouter()


# ================================= #
#          G E T   U S E R          #
# ================================= #

@user.get("/user/query_users", tags=["Query Users"], response_model=dict, status_code=200,
           summary="Consultar todos los Usuarios Activos",
           description="Método para consultar detalles de todos los usuarios")
def query_users_etl_router() -> dict:
        return query_users_etl()
        


# ================================= #
#      C R E A T E   U S E R        #
# ================================= #

@user.post("/user/create_user", tags=["Create User"], response_model=dict, status_code=200, 
        summary="Crear Usuario", 
        description="Método para creación de usuarios")
def create_users_etl_router(create_user: CreateUser) -> dict:
    return create_user_etl(create_user.user_name, create_user.user_last_name, create_user.user_mail, create_user.user_password, create_user.user_rol)


# ================================= #
#      D E L E T E   U S E R        #
# ================================= # 

@user.delete("/user/delete_user", tags=["Delete User"], response_model=dict, status_code=200,
        summary="Eliminar Usuario",
        description="Método para eliminación de usuarios")
def delete_user_etl_router(delete_user: DeleteUser) -> dict:
    return delete_user_etl(delete_user.user_mail)


# ================================= #
#     V A L I D A TE   U S E R      #
# ================================= # 
@user.post("/user/validate_user", tags=["Validate User"], response_model=dict, status_code=200,
        summary="Valida un Usuario",
        description="Método para Validacion de un Usuario")
def validate_user_etl_router(validate_user: ValidateUser) -> dict:
    return validate_user_etl(validate_user.user_mail, validate_user.user_password)



# ================================= #
#   C H A N G E  P A S S W O R D    #
# ================================= # 

@user.post("/user/change_password", tags=["Change Password"], response_model=dict, status_code=200,
        summary="Cambiar credenciales",
        description="Método para Cambiar las credenciales de un Usuario")
def change_password_etl_router(change_pass: ChangePassword) -> dict:
    return change_password_etl(change_pass.user_mail, change_pass.user_password, change_pass.new_password)


# ================================= #
#   R E S E T   P A S S W O R D     #
# ================================= # 

@user.post("/user/reset_password", tags=["Reset Password"], response_model=dict, status_code=200,
           summary="Resetear una contraseña",
           description="Método para resetaer las credenciales de un Usuario")
def reset_password_etl_router(reset_pass: ResetPassword) -> dict:
        return reset_password_etl(reset_pass.user_mail, reset_pass.new_user_password)