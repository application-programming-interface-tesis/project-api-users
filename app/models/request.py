import re
from pydantic import BaseModel, conint, EmailStr
from typing import Optional, List
from enum import Enum

INVALID_MAIL = 'Only the companyec.com domain is allowed'
INVALID_FORMAT = 'Invalid email format'
FIELD_CONTENT = "The field cannot be empty"

def validate_email(email: str) -> bool:
    return email.lower().endswith("@companyec.com")

def validate_email_format(email: str) -> bool:
    return bool(re.match(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$", email))

def common_email_validations(value):
    if not validate_email(value):
        raise ValueError(INVALID_MAIL)
    if not validate_email_format(value):
        raise ValueError(INVALID_FORMAT)
    return value

def common_field_validation(value):
    if not value or value == "string" or value =="":
        raise ValueError(f'El valor {value} no esta permitido')
    return value


# ================================= #
#      C R E A T E   U S E R        #
# ================================= #

class RolType(str, Enum):
    ADM = "ADM"
    USR = "USER"

class CreateUser(BaseModel):
    user_mail: EmailStr
    user_name: str
    user_last_name: str
    user_password: str
    user_rol: RolType
    
    def __init__(self, **data):
        email = data.get("user_mail")
        user_name = data.get("user_name")
        user_last_name = data.get("user_last_name")
        user_password = data.get("user_password")
        
        if email:
            common_email_validations(email)
        if user_name:
            common_field_validation(user_name)
        else:
            raise ValueError(FIELD_CONTENT)
        if user_last_name:
            common_field_validation(user_last_name)
        else:
            raise ValueError(FIELD_CONTENT)
        if user_password:
            common_field_validation(user_password)
        else:
            raise ValueError(FIELD_CONTENT)
        super().__init__(**data)
   
    
# ================================= #
#      D E L E T E   U S E R        #
# ================================= #    

class DeleteUser(BaseModel):
    user_mail: EmailStr
    
    def __init__(self, **data):
        email = data.get("user_mail")
        if email:
            common_email_validations(email) 
        super().__init__(**data)
        
# ================================= #
#     V A L I D A TE   U S E R      #
# ================================= # 

class ValidateUser(BaseModel):
    user_mail: EmailStr
    user_password: str
    
    def __init__(self, **data):
        email = data.get("user_mail")
        user_password = data.get("user_password")
        if email:
            common_email_validations(email)
        if user_password:
            common_field_validation(user_password)
        else:
            raise ValueError(FIELD_CONTENT)
        super().__init__(**data)
        

# ================================= #
#   C H A N G E  P A S S W O R D   #
# ================================= # 

class ChangePassword(BaseModel):
    user_mail: EmailStr
    user_password: str
    new_password: str
    
    def __init__(self, **data):
        email = data.get("user_mail")
        user_password = data.get("user_password")
        new_password = data.get("new_password")
    
        if email:
            common_email_validations(email) 
        if user_password:
            common_field_validation(user_password)
        else:
            raise ValueError(FIELD_CONTENT)
        if new_password:
            common_field_validation(new_password)
        else:
            raise ValueError(FIELD_CONTENT)
        super().__init__(**data)


        
# ================================= #
#   R E S E T   P A S S W O R D     #
# ================================= # 

class ResetPassword(BaseModel):
    user_mail: EmailStr
    new_user_password: str
    
    def __init__(self, **data):
        email = data.get("user_mail")
        new_user_password = data.get("new_user_password")
        if email:
            common_email_validations(email)
        if new_user_password:
            common_field_validation(new_user_password)
        else:
            raise ValueError(FIELD_CONTENT)
        super().__init__(**data)