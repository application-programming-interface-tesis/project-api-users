
import json
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.util.functions import hash_password

config = Config()

def reset_credentials(user_mail, new_user_password):
    try:
        with open(config.PATH_USER_JSON, 'r') as file:
            data = json.load(file)
            
        # Busca el usuario en la lista
        for user_info in data['info_users']:
            if user_info['user_mail'] == user_mail:
                new_credentials, n_salt = hash_password(new_user_password)
                user_info['user_password'] = new_credentials
                user_info['code_password'] = n_salt
                
                return 0
        print('No se encuentra el usuario')
        return 1
    except Exception as ex:
        print('Error reset_credentials: ', ex)    
    

def reset_password_etl (user_mail, new_user_password):
    response = reset_credentials(user_mail, new_user_password)
    if response > 0:
        print('No existe el usuario a resetear')
        resp = {
        "code": 1,
        "message": "No existe el usuario a resetear credenciales"
        }
        return JSONResponse(status_code=400, content=resp)
        
    resp = {
        "code": 0,
        "message": "Credenciales reseteas correctamente"
    }
    return JSONResponse(status_code=200, content=resp)

    
    
        