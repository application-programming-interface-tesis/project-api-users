import json
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.util.functions import hash_password, load_config_user

config = Config()

def user_exists(config_user, user_mail):
    info_users = config_user.get('info_users', [])
    return any(user.get('user_mail') == user_mail for user in info_users)


def add_info_user(user_name, user_last_name, user_alias, user_mail, user_password, role, path_file):
    hashed_password, salt = hash_password(user_password)
    full_name = user_name + ' ' + user_last_name
    new_user = {
        "user_name": user_name,
        "user_last_name": user_last_name,
        "user_full_name": full_name,
        "user_alias": user_alias,
        "user_mail": user_mail,
        "user_password": hashed_password,
        "role": role,
        "code_password": salt
    }
    
    config_user = load_config_user(path_file)
    
    if config_user:
        if user_exists(config_user, user_mail):
            print(f"Error: El usuario '{user_mail}' ya existe.")
            return 1
        
        info_users = config_user.get('info_users', [])
        info_users.append(new_user)
        config_user['info_users'] = info_users

        try:
            with open(path_file, 'w') as file:
                json.dump(config_user, file, indent=2)
                
            print("Nuevo usuario agregado correctamente")
            return 0
        
        except Exception as e:
            print(f"Error al guardar el usuario: {e}")
            return -1
    else:
        print("Error: No se puede agregar información de usuario. Configuración no cargada.")
        return -1


def create_user_etl(user_name: str, user_last_name: str, user_mail: str, password: str, role: str):
    try:
        user_alias = user_mail.split('@')[0]
        response = add_info_user(user_name, user_last_name, user_alias.upper(), user_mail, password, role, config.PATH_USER_JSON)
    
        if response == -1:
            return JSONResponse(status_code=500, content={"code": 1, "message": f"Error al crear el usuario: {user_mail}"})
        elif response == 1:
            return JSONResponse(status_code=400, content={"code": 1, "message": f"El usuario: {user_mail} ya existe"})
        
        message = f'Se creo el usuario {user_mail} correctamente'
        resp = {
            "code": 0,
            "message": message
        }
        return JSONResponse(status_code=200, content=resp)
        
    except Exception as ex:
        print("Error en create_user_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error General: {str(ex)}"})