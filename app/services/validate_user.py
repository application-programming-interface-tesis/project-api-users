import json
from fastapi.responses import JSONResponse
from app.util.functions import hash_password
from app.config.config import Config

config = Config()

def validate_user_etl(usermail, password):
    try:
        with open(config.PATH_USER_JSON, 'r') as file:
            data = json.load(file)
        
        # Busca el usuario en la lista Json
        found = False
        for user_info in data.get("info_users", []):
            if user_info.get("user_mail") == usermail:
                found = True
                # Obtiene el hash de la contrasenia almacenada y el salt asociado
                stored_password_hash = user_info.get('user_password', '')
                stored_salt = user_info.get('code_password', '')
                # Genera el hash de la contrasenia ingresada usando el salt almacenado
                entered_password_hash, ctmp = hash_password(password, stored_salt)
                role = user_info.get("role")
                print('FALSE', ctmp, 'FALSE')
        #SI NO EXISTE EL USUARIO        
        if not found:
            return JSONResponse(status_code=404, content={"code": 1, "message": f"El usuario con el correo {usermail} no existe"})
        #SI NO SON IGUALES LAS CONTRASENIAS
        if stored_password_hash != entered_password_hash:
            return JSONResponse(status_code=200, content={"code": 1, "message": f"Credenciales del usuario {usermail} son incorrectas"})
        
        resp = {
            "code": 0, 
            "message": f"El usuario {usermail} se logueo correctamente",
            "role": role 
        }
        return JSONResponse(status_code=200, content=resp)
            
    except Exception as ex:
        print("Error en validate_user_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error: {str(ex)}"})
        