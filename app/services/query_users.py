import json
from fastapi.responses import JSONResponse
from app.config.config import Config
from fastapi.responses import JSONResponse

def query_users_etl():
    try:
        config = Config()
        if config.PATH_USER_JSON is None:
            raise FileNotFoundError("La ruta del archivo de usuarios no está configurada correctamente.")

        with open(config.PATH_USER_JSON, 'r') as file:
            data = json.load(file)
            
        filtered_data = {
            "info_users": [
                {
                    "user_mail": user["user_mail"],
                    "user_name": user["user_name"],
                    "user_last_name": user["user_last_name"],
                    "user_full_name": user["user_full_name"],                   
                    "user_alias": user["user_alias"],
                    "user_password": user["user_password"],
                    "role": user["role"]
                }
                for user in data["info_users"]
            ]
        }

        resp = {
            "code": 0,
            "message": "Exito",
            "content": filtered_data
        }

        return JSONResponse(status_code=200, content=resp)
    
    except Exception as ex:
        print('query_users_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
