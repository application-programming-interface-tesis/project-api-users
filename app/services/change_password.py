import json
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.util.functions import hash_password

config = Config()

def change_password(user_mail, current_password, new_password):    
    with open(config.PATH_USER_JSON, 'r') as file:
        data = json.load(file)

    # Busca el usuario en la lista
    for user_info in data['info_users']:
        if user_info['user_mail'] == user_mail:
            # Obtiene el hash de la contraseña almacenada y el salt asociado
            stored_password_hash = user_info.get('user_password', '')
            stored_salt = user_info.get('code_password', '')

            # Genera el hash de la contrasenia actual ingresada usando el salt almacenado
            entered_password_hash, _ = hash_password(current_password, stored_salt)

            # Verifica si la contrasenia actual ingresada coincide con la almacenada
            if stored_password_hash == entered_password_hash:
                # Si coincide, genera el hash de la nueva contrasenia y actualiza el archivo JSON
                new_password_hash, new_salt = hash_password(new_password)
                user_info['user_password'] = new_password_hash
                user_info['code_password'] = new_salt

                # Guarda los cambios en el archivo JSON
                with open(config.PATH_USER_JSON, 'w') as file:
                    json.dump(data, file, indent=2)

                print("Contraseña reseteada exitosamente.")
                return 0

    print("Error: Usuario no encontrado o contraseña actual incorrecta.")
    return 1


def change_password_etl(usermail, current_password, new_password):
    try:
        print("usermail: ", usermail)
        print("current_password: ", current_password)
        print("new_password: ", new_password)
        response = change_password(usermail, current_password, new_password)
        if response > 0:
            resp = {
                "code": 1,
                "message": "Usuario no encontrado o contraseña actual incorrecta"
            }
            return JSONResponse(status_code=400, content=resp)
        
        resp = {
                "code": 0,
                "message": "Contraseña reseteada exitosamente"
            }
        return JSONResponse(status_code=200, content=resp)
    except Exception as ex:
        print("Error en change_password_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error: {str(ex)}"})


