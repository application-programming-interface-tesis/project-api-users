import json
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.util.functions import load_config_user

config = Config()

def delete_user_etl(user_mail: str):
    try:
        config_user = load_config_user(config.PATH_USER_JSON)
        if not config_user:
            return JSONResponse(status_code=500, content={"code": 1, "message": "Error al cargar la configuracion de usuarios"})
        
        info_users = config_user.get('info_users', [])
        updated_users = [user for user in info_users if user.get('user_mail') != user_mail]
        
        if len(updated_users) == len(info_users):
            return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encontró el usuario: {user_mail}"})
        
        config_user['info_users'] = updated_users

        with open(config.PATH_USER_JSON, 'w') as file:
            json.dump(config_user, file, indent=2)
            
        resp = {
            "code": 0,
            "message": f'Se elimino el usuario {user_mail} correctamente'
        }
        return JSONResponse(status_code=200, content=resp)
    
    except Exception as ex:
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error General: {str(ex)}"})
