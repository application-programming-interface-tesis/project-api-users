

class ExceptionInternal(Exception):
    def __init__(self, codigo=500, mensaje="Error En el flujo"):
        self.mensaje = mensaje
        self.codigo = codigo
        super().__init__(self.mensaje)